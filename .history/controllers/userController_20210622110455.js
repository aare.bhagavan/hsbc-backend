const userService = require("../services/userService");
const payeesModel = require("../models/payeesModel");

module.exports = {
    addUser : async (req, res) => {
        try {
            if( !req.body.email )    return res.send({ success: false, msg: "Email is required." });
            if( !req.body.password )    return res.send({ success: false, msg: "Password is required." });
            if( !req.body.name )    return res.send({ success: false, msg: "Name is required." });
            if( !req.body.account_number )    return res.send({ success: false, msg: "Account Number is required." });
            let response = await userService.addUser( req.body );
            return res.send(response);
        }
        catch(err) {
            if( !res.headersSent ){
                res.send(err);
            }
        }
    },

    allUsers : async (req, res) => {
        try {
            let response = await userService.allUsers();
            return res.send(response);
        }
        catch(err) {
            if( !res.headersSent ){
                res.send(err);
            }
        }
    },
    getUserdata : async (req, res) => {
        try {
            let response = await userService.getUserdata(req.body);
            return res.send(response);
        }
        catch(err) {
            if( !res.headersSent ){
                res.send(err);
            }
        }
    },


    addpayee : async (req, res) => {
        try {
            if( !req.body.name )    return res.send({ success: false, msg: "Name is required." });
            if( !req.body.ifsccode )    return res.send({ success: false, msg: "IFSC Code is required." });
            if( !req.body.account_number )    return res.send({ success: false, msg: "Account Number is required." });
            // let response = await userService.addpayee( req.body );
            // return res.send(response);
            // console.log('userObjqw>>',req.body)
            // let user = await payeesModel.findOne({ account_number: req.body.account_number }).lean();
            // if (user) return { success: false, msg: "User already exist." };
            let userObj = {
                name: req.body.name,
                email: req.body.email,
                account_number: req.body.account_number,
                mobile_number: req.body.mobile_number,
                ifsccode: req.body.ifsccode,
                confirm_account_number:req.body.confirm_account_number,
                status: req.body.status,
            }
            console.log('userObj123123123123>>',userObj)
            await userModel(userObj).save();
            return { success: true, msg: "User added successfully" };
        }
        catch(err) {
            if( !res.headersSent ){
                res.send(err);
            }
        }
    },
}



