const userService = require("../services/userService");
const payeesModel = require("../models/payeesModel");
const userModel = require("../models/userModel");
module.exports = {
    addUser : async (req, res) => {
        try {
            if( !req.body.email )    return res.send({ success: false, msg: "Email is required." });
            if( !req.body.password )    return res.send({ success: false, msg: "Password is required." });
            if( !req.body.name )    return res.send({ success: false, msg: "Name is required." });
            if( !req.body.account_number )    return res.send({ success: false, msg: "Account Number is required." });
            let usersData = req.body;
            console.log(usersData)
            let user = await userModel.findOne({ account_number: usersData.account_number }).lean();
            if (user) return res.send({ success: false, msg: "User already exist." });
            let userObj = {
                email: usersData.email,
                password: usersData.password,
                name: usersData.name,
                account_number: usersData.account_number,
                account_type: usersData.account_type,
                ifsccode: usersData.ifsccode,
                status: usersData.status,
                customerid: usersData.customerid,
                holder_address: usersData.holder_address,
                bank_address: usersData.bank_address,
                mobile: usersData.mobile,
                state: usersData.state,
                city: usersData.city,
                branch: usersData.branch,
                profile_pic:usersData.profile_pic,
                holder_city: usersData.holder_city,
                holder_state: usersData.holder_state
            }
        await userModel(userObj).save();
        return res.send({ success: true, msg: "User added successfully" });
        }
        catch(err) {
            if( !res.headersSent ){
                res.send(err);
            }
        }
    },

    allUsers : async (req, res) => {
        try {
            let users = await userModel.find({}).lean();
            return res.send({ success: true, data: users });
        }
        catch(err) {
            if( !res.headersSent ){
                res.send(err);
            }
        }
    },
    getUserdata : async (req, res) => {
        try {
            let userdata = await userModel.findOne({ account_number: req.body.account_number}).lean();
            return res.send({ success: true, data: userdata });
        }
        catch(err) {
            if( !res.headersSent ){
                res.send(err);
            }
        }
    },


    
}



