const authService = require("../services/authService");
const userModel = require("../models/userModel");
module.exports = {
    login : async (req, res) => {
        try {
            if( !req.body.email ) return res.send({ success: false, msg: "Email is required." });
            if( !req.body.password ) return res.send({ success: false, msg: "Password is required." });
            let user = await userModel.findOne({ email: email, password: password }).lean();
            if( !user ) return { success: false, msg: "User not exist" };
            return res.send({ success: true, data: user });
            // let response = await authService.login( req.body.email, req.body.password );
            // return res.send(response);
        }
        catch(err) {
            if( !res.headersSent ){
                res.send(err);
            }
        }
    }
}