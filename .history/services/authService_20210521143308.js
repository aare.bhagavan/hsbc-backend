const userModel = require("../models/userModel");

module.exports = {
    login : async ( email, password ) => {
        let user = await userModel.findOne({ email: email, password: password }).lean();
        if( !user ) return { success: false, msg: "User not exist" };
        return { success: true, data: user };
    }
}