const payeesModel = require("../models/payeesModel");
module.exports = {
    
    addpayee : async (req, res) => {
        try {
            if( !req.body.name )    return res.send({ success: false, msg: "Name is required." });
            if( !req.body.ifsccode )    return res.send({ success: false, msg: "IFSC Code is required." });
            if( !req.body.account_number )    return res.send({ success: false, msg: "Account Number is required." });
            let user = await payeesModel.findOne({ account_number: req.body.account_number }).lean();
            if (user) return res.send({ success: false, msg: "User already exist." });
            let userObj = {
                name: req.body.name,
                email: req.body.email,
                account_number: req.body.account_number,
                mobile_number: req.body.mobile_number,
                ifsccode: req.body.ifsccode,
                confirm_account_number:req.body.confirm_account_number,
                status: req.body.status,
                branch: req.body.branch,
                type: req.body.type
            }
            await payeesModel(userObj).save();
            return res.send({ success: true, msg: "User added successfully" });
        }
        catch(err) {
            if( !res.headersSent ){
                res.send(err);
            }
        }
    },

    getAllpayeers: async (req, res) => {
        let users = await payeesModel.find({}).lean();
        return res.send({ success: true, data: users });
    },

    getPayuser : async (req, res) => {
        try {
            let userdata = await payeesModel.findOne({ account_number: req.body.account_number}).lean();
            return res.send({ success: true, data: userdata });
        }
        catch(err) {
            if( !res.headersSent ){
                res.send(err);
            }
        }
    },
}