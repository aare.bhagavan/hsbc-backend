const transactionModel = require("../models/transactionModel");
module.exports = {
    addTransactions : async (req, res) => {
        try {
            console.log(req.body)
            if( !req.body.amount )    return res.send({ success: false, msg: "Amount is required." });
            if( !req.body.ifsccode )    return res.send({ success: false, msg: "IFSC Code is required." });
            if( !req.body.account_number )    return res.send({ success: false, msg: "Account Number is required." });
            if( !req.body.payeer_ifsccode )    return res.send({ success: false, msg: "Payeer IFSC Code is required." });
            if( !req.body.payeer_account_number )    return res.send({ success: false, msg: "Payeer Account Number is required." });
            // let user = await transactionModel.findOne({ account_number: req.body.account_number }).lean();
            // if (user) return res.send({ success: false, msg: "User already exist." });
            let userObj = {
                name: req.body.name,
                email: req.body.email,
                account_number: req.body.account_number,
                ifsccode: req.body.ifsccode,
                mobile_number: req.body.mobile_number,
                payeer_name: req.body.payeer_name,
                payeer_email: req.body.payeer_email,
                payeer_account_number: req.body.payeer_account_number,
                payeer_ifsccode: req.body.payeer_ifsccode,
                payeer_mobile_number: req.body.payeer_mobile_number,
                amount: req.body.amount,
                branch: req.body.branch,
                remark: req.body.remark,
                type: req.body.type
              }
            await transactionModel(userObj).save();
            return res.send({ success: true, msg: "Transactions Done successfully" });
        }
        catch(err) {
            if( !res.headersSent ){
                res.send(err);
            }
        }
    },
    getAllTransaction: async (req, res) => {
        let users = await transactionModel.find({}).lean();
        return res.send({ success: true, data: users });
    },
}
