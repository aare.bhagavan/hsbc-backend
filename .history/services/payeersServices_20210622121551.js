const payeesModel = require("../models/payeesModel");
module.exports = {
    getAllpayeers: async (req, res) => {
        let users = await payeesModel.find({}).lean();
        return res.send({ success: true, data: users });
    },
}