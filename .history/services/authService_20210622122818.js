const userModel = require("../models/userModel");

module.exports = {
    login : async ( req, res ) => {
        let user = await userModel.findOne({ email: req.body.email, password: req.body.password }).lean();
        if( !user ) return { success: false, msg: "User not exist" };
        return { success: true, data: user };
    }
}