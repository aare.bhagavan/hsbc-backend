const userModel = require("../models/userModel");
module.exports = {
    addUser: async ({ email,password,name,account_number,account_type,ifsccode,status,customerid,holder_address,
        bank_address,mobile,profile_pic,state,city,branch,holder_city,holder_state, }) => {
        let user = await userModel.findOne({ account_number: account_number }).lean();
        if (user) return { success: false, msg: "User already exist." };

        let userObj = {
            email: email,
            password: password,
            name: name,
            account_number: account_number,
            account_type: account_type,
            ifsccode: ifsccode,
            status: status,
            customerid: customerid,
            holder_address: holder_address,
            bank_address: bank_address,
            mobile: mobile,
            state: state,
            city: city,
            branch: branch,
            profile_pic:profile_pic,
            holder_city: holder_city,
            holder_state: holder_state
        }

        await userModel(userObj).save();
        return { success: true, msg: "User added successfully" };
    },

    allUsers: async () => {
        let users = await userModel.find({}).lean();
        return { success: true, data: users };
    },
    getUserdata: async ({account_number}) => {
        let userdata = await userModel.findOne({ account_number: account_number}).lean();
        return { success: true, data: userdata };
    },


}