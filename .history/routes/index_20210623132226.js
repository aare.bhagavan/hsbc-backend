const router = require("express").Router();
const userService = require("../services/userService");
const authService = require("../services/authService");
const payeersServices = require("../services/payeersServices");
const transactionsServices = require("../services/transactionsServices");
router.route('/auth').post(authService.login);
router.route('/users').post(userService.addUser)
router.route('/getAllusers').get(userService.allUsers)
router.route('/getusers').post(userService.getUserdata);
router.route('/addpayee').post(payeersServices.addpayee);
router.route('/getAllpayeers').get(payeersServices.getAllpayeers);
router.route('/getpayuser').post(payeersServices.getPayuser);
router.route('/addTransactions').post(transactionsServices.addTransactions)
router.route('/getAllTransaction').get(transactionsServices.getAllTransaction)
module.exports = router;