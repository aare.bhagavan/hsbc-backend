const router = require("express").Router();
const userController = require("../controllers/userController");
const authController = require("../controllers/authController");

router.use('/auth').post(authController.login);
router.use('/users', require('./userRouter'));
router.use('/getusers', require('./userDataRoute'));
router.route('/addpayee').post(userController.addpayee);
module.exports = router;