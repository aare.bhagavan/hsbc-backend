const router = require("express").Router();
const userController = require("../controllers/userController");
const authController = require("../controllers/authController");

router.route('/auth').post(authController.login);
router.use('/users', require('./userRouter'));
router.route('/getusers').post(userController.getUserdata);
router.route('/addpayee').post(userController.addpayee);
router.route('/getAllpayeers').get(userController.addpayee);
module.exports = router;