const router = require("express").Router();
const userController = require("../controllers/userController");
router.use('/auth', require('./authRouter'));
router.use('/users', require('./userRouter'));
router.use('/getusers', require('./userDataRoute'));
router.route('/addpayee').post(userController.addpayee);
module.exports = router;