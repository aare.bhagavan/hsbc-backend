const router = require("express").Router();
const userController = require("../controllers/userController");
const authController = require("../controllers/authController");
const payeersServices = require("../services/payeersServices");
router.route('/auth').post(authController.login);
router.use('/users', require('./userRouter'));
router.route('/getusers').post(userController.getUserdata);
router.route('/addpayee').post(payeersServices.addpayee);
router.route('/getAllpayeers').get(payeersServices.getAllpayeers);
module.exports = router;