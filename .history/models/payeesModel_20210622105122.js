const mongoose = require("mongoose");

const payeers = mongoose.Schema({
  name: { type: String, default: "user" },
  email: { type: String },
  account_number: { type: Number },
  ifsccode: { type: String },
  start_date: { type: Date, default: Date.now() },
  confirm_account_number: { type: Number },
  mobile_number: { type: String, default: "" },
  status: { type: String, default: "active" },
});

module.exports = mongoose.model("payeer", Payeers);
