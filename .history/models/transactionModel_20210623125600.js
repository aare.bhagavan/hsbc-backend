const mongoose = require("mongoose");

const Transaction = mongoose.Schema({
  name: { type: String, default: "user" },
  email: { type: String },
  account_number: { type: Number },
  ifsccode: { type: String },
  send_date: { type: Date, default: Date.now() },
  mobile_number: { type: String, default: "" },
  status: { type: String, default: "pending" },
  payeer_name: { type: String, default: "user" },
  payeer_email: { type: String },
  payeer_account_number: { type: Number },
  payeer_ifsccode: { type: String },
  send_date: { type: Date, default: Date.now() },
  mobile_number: { type: String, default: "" },
  amount: { type: Number },
  branch: { type: String },
});

module.exports = mongoose.model("transaction", Transaction);
