const express = require("express");
const cookieParser = require("cookie-parser");
const hoganExpress = require("hogan-express");
const chalk = require("chalk");
const morgan = require('morgan');

const config = require("./config/config");
const router = require("./routes");

require("./config/dbConnect").connectToDb();

const app = express();
const port = config.port;

app.use(express.static('dist'));
app.use('/public',express.static('public'));

app.engine('html', hoganExpress);
app.set('view options', { layout: true });
app.set('layout', 'container');
app.set('views', __dirname + '/dist');
app.set('view engine', 'html');

app.use(express.text({ limit: config.bodyParserLimit }));
app.use(express.raw({ limit: config.bodyParserLimit }));
app.use(express.json({ limit: config.bodyParserLimit }));
app.use(express.urlencoded({ extended: true, limit: config.bodyParserLimit, parameterLimit: config.parameterLimit }));

app.use(morgan( (tokens, req, res) => {
        let method = tokens.method(req, res);
        let url = tokens.url(req, res);
        let resptime = `${tokens['response-time'](req, res)} ms`;
        return `${chalk.bold.greenBright(method)} ${chalk.yellowBright(url)}  ${chalk.yellowBright(resptime)}`;
    })
)

app.use(cookieParser());

app.use( (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader("Access-Control-Allow-Credentials", true);
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
    next();
});

app.use('/', router);

app.use( (req, res, next) => {
    return res.render('index');
});

app.listen(port, () => {
    console.log(`Dashboard running on ${port}`);
})