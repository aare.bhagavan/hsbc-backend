config = {};

config.port = process.env.PORT || 3000;

config.dbUrl = `mongodb://localhost:27017/hsbc`     //change db name instead of test and localhost to your mongo server

module.exports = config;