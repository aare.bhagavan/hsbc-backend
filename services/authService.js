const userModel = require("../models/userModel");

module.exports = {
    login : async ( req, res ) => {
        let user = await userModel.findOne({ email: req.body.email, password: req.body.password }).lean();
        if( !user ) return res.send({ success: false, msg: "User not exist" });
        return res.send({ success: true, data: user });
    }
}