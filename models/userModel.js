const mongoose = require("mongoose");

const User = mongoose.Schema({
  email: { type: String },
  password: { type: String },
  name: { type: String, default: "user" },
  account_number: { type: Number },
  account_type: { type: String },
  ifsccode: { type: String },
  start_date: { type: Date, default: Date.now() },
  status: { type: String, default: "active" },
  amount: { type: Number },
  customerid: { type: String },
  holder_address: { type: String },
  holder_city: { type: String },
  holder_state: { type: String },
  bank_address: { type: String },
  state: { type: String },
  city: { type: String },
  branch: { type: String },
  profile_pic: { type: String },
  mobile: { type: String, default: "" },
  balance_amount: {type: Number},
  isExpand:{type: Boolean, default: false}
});

module.exports = mongoose.model("users", User);
